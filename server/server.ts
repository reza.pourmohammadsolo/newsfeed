const jsonServer = require('json-server');
const server = jsonServer.create();
const router = jsonServer.router('server/db.json');
const middlewares = jsonServer.defaults();
const db = require('./db.json');
const fs = require('fs');
server.use(middlewares);
server.use(jsonServer.bodyParser);
server.use('/my', (req, res, next) => {
  const users = readUsers();
  const user = users[0];
  if (user) {
    res.send({ user, token: 'valid-token' });
  } else {
    res.status(401).send('Incorrect username or password');
  }
});
server.post('/friends', (req, res, next) => {
  if (isAuthorized(req)) {
    const AllFriends = readFriends();
    const friends = AllFriends.filter(
      u => u.id + '' === req.body.userId)[0];
    res.send(friends);
  } else {
    res.sendStatus(401);
  }
});
server.post('/posts', (req, res, next) => {
  if (isAuthorized(req)) {
    const AllPosts = readPosts();
    const posts = AllPosts.filter(
      u => u.userId + '' === req.body.userId);
    res.send(posts);
  } else {
    res.sendStatus(401);
  }
});
server.post('/userInfo', (req, res, next) => {
  if (isAuthorized(req)) {
    const Users = readUsers();
    const user = Users.filter(
      u => u.id  === req.body.userId)[0];
    res.send(user);
  } else {
    res.sendStatus(401);
  }
});
server.post('/news', (req, res, next) => {
  if (isAuthorized(req)) {
    const AllNews = readNews();
    res.send(AllNews);
  } else {
    res.sendStatus(401);
  }
});
server.use(router);
server.listen(3000, () => {
  console.log('JSON Server is running');
});

function isAuthorized(req) {
  return req.headers.authorization === 'valid-token';
}

function readUsers() {
  const dbRaw = fs.readFileSync('./server/db.json');
  return JSON.parse(dbRaw).users;
}
function readFriends() {
  const dbRaw = fs.readFileSync('./server/db.json');
  return JSON.parse(dbRaw).friends;
}
function readNews() {
  const dbRaw = fs.readFileSync('./server/db.json');
  return JSON.parse(dbRaw).news;
}
function readPosts() {
  const dbRaw = fs.readFileSync('./server/db.json');
  return JSON.parse(dbRaw).posts;
}
