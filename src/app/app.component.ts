import { Component } from '@angular/core';
import {AuthServiceService} from './auth-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'newsfeed-app';
  constructor(private authService: AuthServiceService) {
    this.authService.getUserInfo().subscribe(data => {
      localStorage.setItem('token' , data.token);
      localStorage.setItem('id' , data.user.id);
      localStorage.setItem('username' , data.user.username);
      localStorage.setItem('name' , data.user.name);
      localStorage.setItem('lastName' , data.user.lastName);
      localStorage.setItem('phone' , data.user.phone);
      localStorage.setItem('email' , data.user.email);
    });
  }
}
