import { Injectable } from '@angular/core';
import {APIUrl} from '../Global';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Post} from '../Models/post';
import {catchError, map} from 'rxjs/operators';
import {News} from '../Models/news';

@Injectable({
  providedIn: 'root'
})
export class NewsServiceService {

  url = `${APIUrl}`;
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: localStorage.getItem('token') })
  };
  constructor(private http: HttpClient) { }
  getNews(id): Observable<News[]>{
    return this.http.post<News[]>(this.url + '/news', {userId: id}, this.httpOptions )
      .pipe(
        map((data: News[]) => {
          return data;
        }), catchError( error => {
          return throwError( error );
        })
      );
  }
}
