import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {News} from '../Models/news';
import {NewsServiceService} from './news-service.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  id;
  news: News[];
  constructor(private activatedRoute: ActivatedRoute , private newsService: NewsServiceService) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => {this.id = params.id; });
    this.newsService.getNews(this.id).subscribe(data => {
      this.news = data;
    });
  }

}
