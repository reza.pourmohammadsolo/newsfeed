import {Component, Input, OnInit} from '@angular/core';
import {News} from '../../Models/news';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.scss']
})
export class NewsCardComponent implements OnInit {
  @Input() newItem: News;
  tempDate;
  date;
  time;
  newsTypes = { 1 : 'Posted a new Post' , 2 : 'Liked a Post of ' , 3 : 'Added new friend'};
  constructor() { }

  ngOnInit(): void {
    this.tempDate = new Date(this.newItem.happenDate);
    this.time = this.tempDate.getHours() + ':' + this.tempDate.getMinutes();
    this.date = this.tempDate.getFullYear() + '/' + this.tempDate.getMonth() + '/' + this.tempDate.getDay();
  }

}
