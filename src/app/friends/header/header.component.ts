import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {AuthServiceService} from '../../auth-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() public sidenavToggle = new EventEmitter();
  name;
  constructor(private authService: AuthServiceService) { }

  ngOnInit(): void {
    this.name = localStorage.getItem('name') + '  ' + localStorage.getItem('lastName');
  }
  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

}
