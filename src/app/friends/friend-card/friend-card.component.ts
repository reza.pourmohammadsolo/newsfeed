import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../Models/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-friend-card',
  templateUrl: './friend-card.component.html',
  styleUrls: ['./friend-card.component.scss']
})
export class FriendCardComponent implements OnInit {
  @Input() isOnline;
  @Input() user: User;
  constructor(private  router: Router) { }
  ngOnInit(): void {
  }
  redirectTo(uri){
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
      this.router.navigate([uri]));
  }

}
