import { Injectable } from '@angular/core';
import {APIUrl} from '../Global';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {User} from '../Models/user';

@Injectable({
  providedIn: 'root'
})
export class FriendsServiceService {

  url = `${APIUrl}`;
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: localStorage.getItem('token') })
  };
  constructor(private http: HttpClient) { }
  getFriends(id): Observable<any>{
    return this.http.post<any>(this.url + '/friends', {userId: id}, this.httpOptions )
      .pipe(
        map((data: any) => {
          return data;
        }), catchError( error => {
          return throwError( error );
        })
      );
  }
  getUserInfo(id): Observable<User>{
    return this.http.post<User>(this.url + '/userInfo', {userId: id}, this.httpOptions )
      .pipe(
        map((data: User) => {
          return data;
        }), catchError( error => {
          return throwError( error );
        })
      );
  }
}
