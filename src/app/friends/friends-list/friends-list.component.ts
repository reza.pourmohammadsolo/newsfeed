import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {FriendsServiceService} from '../friends-service.service';
import {User} from '../../Models/user';

@Component({
  selector: 'app-friends-list',
  templateUrl: './friends-list.component.html',
  styleUrls: ['./friends-list.component.scss']
})
export class FriendsListComponent implements OnInit {
  @Output() sidenavClose = new EventEmitter();
  friends: User[];
  constructor(private friendsService: FriendsServiceService) {}

  ngOnInit(): void {
    this.friendsService.getFriends(localStorage.getItem('id')).subscribe(data => {
      this.friends = new Array(0);
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < data.friends.length ; i ++) {
        this.friendsService.getUserInfo(data.friends[i].id).subscribe(result => {
          this.friends.push(result);
        });
      }
    });
  }
  public onSidenavClose = () => {
    this.sidenavClose.emit();
  }

}
