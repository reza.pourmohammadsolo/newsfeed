import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {APIUrl} from './Global';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  url = `${APIUrl}`;
  constructor(private http: HttpClient) { }
  getUserInfo(): Observable<any>{
    return this.http.get<any>(this.url + '/my', )
      .pipe(
        map((data: any) => {
          return data;
        }), catchError( error => {
          return throwError( error );
        })
      );
  }
}
