import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NewsComponent} from './news/news.component';
import {PostsComponent} from './posts/posts.component';


const routes: Routes = [
  { path: 'news', component: NewsComponent,
    loadChildren: () => import(`./news/news.module`).then(m => m.NewsModule) },
  { path: 'posts/:id', component: PostsComponent,
    loadChildren: () => import(`./posts/posts.module`).then(m => m.PostsModule) },
  { path: '',   redirectTo: 'posts', pathMatch: 'full' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
