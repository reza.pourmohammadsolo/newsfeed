import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostsRoutingModule } from './posts-routing.module';
import { PostsComponent } from './posts.component';
import { PostCardComponent } from './post-card/post-card.component';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [PostsComponent, PostCardComponent],
  imports: [
    CommonModule,
    PostsRoutingModule,
    MatCardModule,
    MatIconModule
  ]
})
export class PostsModule { }
