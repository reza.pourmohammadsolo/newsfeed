import { Injectable } from '@angular/core';
import {APIUrl} from '../Global';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Post} from '../Models/post';

@Injectable({
  providedIn: 'root'
})
export class PostsServiceService {

  url = `${APIUrl}`;
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: localStorage.getItem('token') })
  };
  constructor(private http: HttpClient) { }
  getPosts(id): Observable<Post[]>{
    return this.http.post<Post[]>(this.url + '/posts', {userId: id}, this.httpOptions )
      .pipe(
        map((data: Post[]) => {
          return data;
        }), catchError( error => {
          return throwError( error );
        })
      );
  }
}
