import { Component, OnInit } from '@angular/core';
import {PostsServiceService} from './posts-service.service';
import {ActivatedRoute} from '@angular/router';
import {Post} from '../Models/post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  id;
  posts: Post[];
  constructor(private activatedRoute: ActivatedRoute , private postService: PostsServiceService) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => {this.id = params.id; });
    this.postService.getPosts(this.id).subscribe(data => {
      this.posts = data;
    });
  }

}
