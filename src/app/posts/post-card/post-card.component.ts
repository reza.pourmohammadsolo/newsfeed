import {Component, Input, OnInit} from '@angular/core';
import {Post} from '../../Models/post';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss']
})
export class PostCardComponent implements OnInit {
  @Input() post: Post;
  tempDate;
  date;
  time;
  constructor() { }

  ngOnInit(): void {
    this.tempDate = new Date(this.post.happenDate);
    this.time = this.tempDate.getHours() + ':' + this.tempDate.getMinutes();
    this.date = this.tempDate.getFullYear() + '/' + this.tempDate.getMonth() + '/' + this.tempDate.getDay();
  }

}
